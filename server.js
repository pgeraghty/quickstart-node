var express = require('express');
var app = express();
//var redis = require("redis");

var r;
//var options = {
//	max_attempts : 3
//};
var redisConnected = false;

r = require('./redis');

r.on('error', function(err){
	console.log('Error on redis', err);
});

r.on('connect', function(){
	redisConnected = true;
});

app.get('/', function (req, res) {
	if(!redisConnected){
		return res.send('No connection to redis');
	}

	r.incr('counter', function (err, data) {
		if(err){return res.status(500).send(err);}

		res.send('Visits '+ data);
	});
});

app.get('/env', function (req, res) {
	res.json( process.env );
});

var server = app.listen(process.env.PORT || 3000, function () {
});
var host = server.address().address;
var port = server.address().port;	
console.log('Example app listening at http://%s:%s', host, port);