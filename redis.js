/**
  * Module dependencies.
  */
var redis = require('redis-cluster').clusterClient;

// expose

module.exports = function(done){
  if( process.env.QUICKSTARTNODE_REDIS_1_PORT && process.env.QUICKSTARTNODE_REDIS_1_ENV_REDIS_PASS ){
	options.auth_pass = process.env.QUICKSTARTNODE_REDIS_1_ENV_REDIS_PASS;
  }

  if( process.env.QUICKSTARTNODE_REDIS_1_PORT && process.env.QUICKSTARTNODE_REDIS_1_PORT_6379_TCP_ADDR ){
	var port = process.env.QUICKSTARTNODE_REDIS_1_PORT_6379_TCP_PORT;
	var host = process.env.QUICKSTARTNODE_REDIS_1_PORT_6379_TCP_ADDR;
	var addr = host + ':' + port;
    console.log('Attempting Redis connection: %s', addr);
	//r = redis.createClient(port, host, options);
	new redis.clusterInstance(addr, done); // firstRedisNode
  }
};